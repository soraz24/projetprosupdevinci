const express = require('express');
const bodyParser = require('body-parser');
const apiHelper = require('./helpers/apiHelpers');
const Web3 = require('web3');
const web3 = new Web3('ws://localhost:7545');
const JobContract = require('../projetpro/src/abis/Projetpro.json');
const request = require('request');

const app = express();

app.use(async function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');

    next();
});

app.use(bodyParser.json());

app.get('/api/jobs', async function(req, res, next) {
    const networkId = await web3.eth.net.getId().catch(() => res.status(500).json({msg: 'cannot access to networkId'}));
    const networkData = JobContract.networks[networkId];
    if (networkData) {
        const jobContract = new web3.eth.Contract(JobContract.abi, networkData.address);
        //writeLog("fetching all pipelines");
        jobContract
            .getPastEvents('JobCreated', {fromBlock: 0, toBlock: 'latest'})
            .then(events => {
                // filtrage des données de l'event pour ne garder que les données des jobs
                //writeLog(events.length + " jobs");
                const jobs = events.map(event => fromEvent(event));
                res.status(200).send(jobs);
            })
            .catch(err => res.status(500).json({error: JSON.stringify(err)}));
    }
});

app.post('/api/jobs', async function(req,res, next) {
    request({
        url: 'https://gitlab.com/api/v4/projects/22288816/jobs',
        headers: {'Authorization': 'Bearer gTBsdjZCrpxRVL8sRnzK'},
        rejectUnauthorized: false
    }, async function (error, response) {
        if (!error) {
            const jobs = JSON.parse(response.body);
            const accounts = await web3.eth.getAccounts().catch(() => res.status(500).json({msg: 'error while getting accounts'}));
            const networkId = await web3.eth.net.getId();
            const networkData = JobContract.networks[networkId];
            if (networkData && jobs && jobs.length > 0) {
                const jobContract = new web3.eth.Contract(JobContract.abi, networkData.address);
                const jobCreateds = [];
                jobs
                    .map(job => fromGitlab(job))
                    .forEach(job =>
                        estimateGasPromise(jobContract, job)
                            .then(gas =>
                                sendJob(jobContract, accounts[0], job, gas)
                                    .then(onResolved => {
                                        jobCreateds.push(onResolved.events.JobCreated.returnValues);
                                        if (jobCreateds.length === jobs.length) {
                                            res.status(201).send(jobCreateds);
                                        }
                                    }))
                            .catch(err => res.status(500).json(err))
                    );
            }
        }
    });
});

estimateGasPromise = (contract, job) => {
    return contract
        .methods
        .createJob(job.jobId, job.status, job.ref, job.commit,  job.pipelineId, job.pipelineStatus)
        .estimateGas()
}

sendJob = async function (contract, account, job, gas) {
    return contract
        .methods
        .createJob(job.jobId, job.status, job.ref, job.commit,  job.pipelineId, job.pipelineStatus )
        .send({from: account, gas: gas})
}

fromEvent = jobCreatedEvent => ({
    jobId: jobCreatedEvent.returnValues.jobId,
    status: jobCreatedEvent.returnValues.status,
    ref: jobCreatedEvent.returnValues.ref,
    commit: jobCreatedEvent.returnValues.commit,
    pipelineId: jobCreatedEvent.returnValues.pipelineId,
    pipelineStatus: jobCreatedEvent.returnValues.pipelineStatus
});

fromGitlab = apiJobs => ({
    jobId: apiJobs.id || '',
    status: apiJobs.status || '',
    ref: apiJobs.ref || '',
    commit: apiJobs.commit.id || '',
    pipelineId: apiJobs.pipeline.id || '',
    pipelineStatus: apiJobs.pipeline.status || ''
    
    //branch: apiJobs.ref || '',
    //stage: apiJobs.stage || '',
    //name: apiJobs.name || '',
    //pipelineStatus: apiJobs.pipeline.status || ''
});

module.exports = app;