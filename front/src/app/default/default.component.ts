import { Component, OnDestroy, OnInit } from '@angular/core';
import { StateService } from '../services/state.service';
import { StuffService } from '../services/stuff.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Job } from '../models/Job.model';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit, OnDestroy {

  public jobs: Job[] = [];
  public loading: boolean;
  cols: any[];

  private jobSub: Subscription;

  constructor(private state: StateService,
    private stuffService: StuffService,
    private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.state.mode$.next('list');
    this.jobSub = this.stuffService.job$.subscribe(
      (job) => {
        this.jobs = job;
        this.loading = false;
      }
    );
    this.stuffService.getJob();
  }

  onNavigate(endpoint: string) {
    this.router.navigate([endpoint]);
  }

  ngOnDestroy() {
    this.jobSub.unsubscribe();
  }
}
