import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Job } from '../models/Job.model';

@Injectable({
  providedIn: 'root'
})
export class StuffService {

  constructor(private http: HttpClient) {}

  private job: Job[] = [];
  public job$ = new Subject<Job[]>();

  getJob() {
    this.http.get('http://localhost:3000/api/jobs').subscribe(
      (stuff: Job[]) => {
        if (stuff) {
          this.job = stuff;
          this.emitJob();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  emitJob() {
    this.job$.next(this.job);
  }
}
