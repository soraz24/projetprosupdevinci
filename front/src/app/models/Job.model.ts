export class Job {
    jobId: number;
    status: string;
    ref: string;
    commit: string;
    pipelineId: number;
    pipelineStatus: string;
  }
  