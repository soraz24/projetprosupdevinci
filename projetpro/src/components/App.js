import React, { Component } from 'react';
import Web3 from 'web3';
import logo from '../logo.png';
import './App.css';
import Projetpro from '../abis/Projetpro.json';
import Navbar from './Navbar';
import Main from './Main';

class App extends Component {

  async componentWillMount(){
    await this.loadWeb3()
    await this.loadBlockchainData
  }
  
  async loadWeb3(){

    if (window.ethereum) {
        window.web3 = new Web3(window.ethereum)
        await window.ethereum.enable()
    }
    
    else if (window.web3) {
        window.web3 = new Web3(window.web3.currentProvider)
    }
    
    else {
        window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }

  }

async loadBlockchainData(){
  const Web3 = window.web3

  //load account
const accounts = await Web3.eth.getAccounts()
this.setState({account: accounts[0]})
const networkId = await Web3.eth.net.getId()
const networkData =Projetpro.networks[networkId]
if (networkData) {
  const projetpro =Web3.eth.Contract(Projetpro.abi, networkData.address)
  this.setState({projetpro })
  const jobCount = await projetpro.methods.jobCount().call()
  this.setState({jobCount})
  //Load products and fetch existing one
  for (var i = 1; i <= jobCount; i++) {
    const displayjob = await projetpro.methods.displayjobs(i).call()
    this.setState({
      displayjob: [...this.state.displayjobs,displayjob]
    })
  }
  this.setState({loading: false})
}else{
  window.alert('Projetpro Contract not deployed to detected network.')
}
  
}

constructor(props){
  super(props)
  this.state = {
    account: '',
    jobCount:0,
    products: [],
    loading: true
  }

  //this.createProduct = this.createProduct.bind(this)
   //this.purchaseProduct = this.purchaseProduct.bind(this)
}
  
 createJob(status, pipeline, commit){

    this.setState({ loading: true})
    this.state.projetpro.methods.createJob(status, pipeline, commit).send({ from: this.state.account }).once('receipt',(receipt)=>{
      this.setState({loading:false})
    })
  }

  /*purchaseProduct(id, price){

    this.setState({ loading: true})
    this.state.Projetpro.methods.purchaseProduct(id).send({ from: this.state.account, value: price })
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
    })
  }*/



  render() {
    return (
      <div>
      <Navbar account={this.state.account}/>
        <div className="container-fluid mt-5">
        <div className="row">
        <main role="main" className="col-lg-12 d-flex">
       <Main  displayjobs={this.state.displayjobs} 
              createJob={this.createJob} 
              /*purchaseProduct={this.createProduct}*//>
        </main>
        </div>
        </div>
      </div>
    );
  }
}

export default App;
