pragma solidity ^0.5.0;

contract Projetpro{
	string public name;
	uint public jobCount = 0; //number of product in the blockchain

	mapping(uint=> Displayjob) public displayjobs;

	struct Displayjob{
	uint id;
	uint jobId;
	string status;
	string ref;
	string commit;
	uint pipelineId;
	string pipelineStatus;
	}


//create event
event JobCreated(
		uint jobId,
		string status,
		string ref,
		string commit,
		uint pipelineId,
		string pipelineStatus
	);

constructor() public{
name = "apprentissage Blockchain Projetpro";
}



function createJob(uint  _jobId, string memory _status, string memory _ref,
					 string memory _commit, uint  _pipelineId, string memory _pipelineStatus) public {

//Increment job count
jobCount++;
// create the job in blockchain
displayjobs[jobCount] = Displayjob(jobCount, _jobId, _status,  _ref,  _commit, _pipelineId, _pipelineStatus);

//Trigger an event
emit JobCreated(_jobId, _status, _ref,  _commit, _pipelineId, _pipelineStatus);	
}
}